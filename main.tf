provider "aws" {
	region = "us-east-1"	 
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDGemp6r21fkLOYvtlrJ/Z/t7o/eAx3FhddmNWWVrzomSV/rYHqTgTWcjUNlxf/0xfuigPLQbSFOoaFwebX/6lqe0EPiDIgR6zVvmzxLRVqSMjOpX2WgIE6AJQqvHYa60RIAhLXlN3JV+CSWWWI8V9+sUOLAINYrOo3sp6wHz/LZXd3Qga/Qquq5uwsBrss6r7kH0uCT5X9RC2OvuirrAnlABw8+Lgu44H1pQEJgcXMK2XcZuHPEKOCnY2oRoxAUpeDWl/zBcvOyJBrdfw0YggH6ogW7+GskNLvOCzBmebpfF4l0pT/CWTauSNhtp5uwKwSGexeyJGZ8xYU3R5u4jgF rsa-key-20211201"
}

module "sg_module" {
  source = "./EC2_Module"
}

module "ec2_module" {
  sg_id = "${module.sg_module.sg_output_id}"
  source = "./EC2_Module"
}

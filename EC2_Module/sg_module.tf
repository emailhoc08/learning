variable "vpcid" {
  type = string
  default = "vpc-0ff0194d756822895"
}

resource "aws_security_group" "sg_module" {
  name        = "sg_module""
  description = "Security Group Module"
  vpc_id      = "${var.vpcid}"
  ingress {
    description      = "SSH inbound rule"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description      = "HTTP inbound rule"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }  
}
output "sg_output_id" {
    value = "${aws_security_group.sg_module.id}"
}

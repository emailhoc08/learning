provider "aws" {
  region = "us-east-1"   
}

variable "amiid" {
  default = "ami-0ed9277fb7eb570c9" 
}

variable "sg_id" {}

resource "aws_instance" "terraform-ec2" {
  ami           = "${var.amiid}" 
  instance_type = "t2.micro"
  key_name = "deployer-key"
  vpc_security_group_ids = ["${var.sg_id}"] 
  tags = {
    Name = "Terraform EC2 Instance" 
  }
}
